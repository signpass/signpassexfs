/**
 * Created by Mattan Shpaier on 2/5/2018.
 */

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/")
	.then(function () {
		console.log("local db is connected");
	},
		function (err) {
			if (err) {
				console.err("local db is not connected: " + err);
			}
		});


const disconnect = () => {
	mongoose.disconnect()
		.then(() => {
			console.log('Disconnected from DB')
		})
}


var UserSchema = new mongoose.Schema({
	//TODO: add here the schema's fields needed.
})

// Example of how to add a static function to a Schema.
// This function implementation is not important this exercise because you will not use it.
UserSchema.statics.remove = function remove(userName) {
    return this.findOneAndRemove({ name: userName })
        .then(user => {
            if(user) {
            }
        })
}


module.exports = mongoose.model('User', UserSchema)

