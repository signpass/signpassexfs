var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var users = require('./users')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'old.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.json({limit: '1mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.static("./",{index:'public/index.html'}));

//TODO: second part of the ex goes here.
app.post('/register', function(req,res){

});


var port = process.env.port || 3001;
var address = process.env.address;

// Set Express to listen out for HTTP requests
var server = app.listen( port,address, function () {
    console.log(`Timmy is listening on port ${server.address().port}`)
});

module.exports = app;
